
//
// Created by david on 09/04/2017.
//

/** @file individuo.hh
 * @brief Especificacio clase individuo
 */

#ifndef PRO2_INDIVIDUO_H
#define PRO2_INDIVIDUO_H

#ifndef NO_DIAGRAM
#include <iostream>
#include <vector>
using namespace std;
#endif

#include "cromosoma.hh"

    /**@class individuo
     *
     * @brief
     * Classe individuo.
     * Representa un individu de la especie
     *
     */

class individuo {
private:
    //Familia
    string nomMare;
    string nomPare;
    string nom;

    bool pares;
    string sexe;


    pair<cromosoma,cromosoma> cromosomasSexuales;
    vector<pair<cromosoma,cromosoma>> cromosomasNormales;

    /**@brief  Operacio de creuament dels cromosomes
     *
     * @param primer
     * @param segon
     * @param puntDeTall
     * @param midaMax
     *
     * \pre
     * <strong>midaMax</strong> i <strong>puntDeTall</strong> son mes petits que la mida del cromosoma <strong>primer</strong> i <strong>segon</strong>
     * \post
     * El valor desde <strong>puntDeTall</strong> fins <strong>midaMax</strong> dels cromosomes <strong>primer</strong> i <strong>segon</strong> queda intercambiat.
     *
     */
    void creuarCromosomes(cromosoma &primer, cromosoma &segon, int puntDeTall, int midaMax);

public:
    /** @brief Constructora
     *
     */
    individuo();

    /** @brief Constructora de individu sense pares
     *
     * @param NumCromosomas
     * @param longCromosomas
     * @param ly
     * @param lx
     * @param nom
     *
     * \pre
     * <em>Cert</em>
     * \post
     * Crea un individu nou sense pares de nom <strong>nom</strong> amb tants cromosomes com <strong>NumCromosomes</strong>. Els cromosomes normals obtenen la longitud
     * del valor de <strong>longCromosomes</strong> en la posicio del cromosoma. El primer cromosoma sexual es de tipus X amb longitud <strong>lx</strong> i el segon te el valro que indiquis amb longitud <strong>lx</strong>/<strong>ly</strong> depenent qeu hagis indicat.
     *
     */
    individuo(const int NumCromosomas,const vector<int>& longCromosomas,const int ly,const int lx,const string nom);

    /**@brief Constructora de individu amb pares
     *
     * @param madre
     * @param padre
     * @param nom
     * @param longCromosoma
     * @param numCromosomes
     * @param l0
     *
     * \pre
     * <em>Cert</em>
     * \post
     * Crea un individu nou de nom <strong>nom</strong> amb tants cromosomes com tenen els pares(<strong>numCromosomes</strong>) amb la seva longitud.
     * Els cromosomes del nou individu obtenen el valor de un dels cromosomes dels pares de cada par i despres entrecreuant els cromosomes.
     *
     */
    individuo(const individuo madre,const individuo padre,const string nom,const vector<int>& longCromosoma,const int numCromosomes, const int l0);


    //Destructora
    /**@brief Destructora
     *
     */
    ~individuo();

    //Return
    /**@brief Operacio de consulta del nom del individu en p.i.
     *
     * @return
     *
     * \pre
     * <em>Cert</em>
     * \post
     * Retorna el nom del individu en p.i.
     *
     */
    string retornNom() const;

    /**@brief Operacio de consulta del sexe del individu en p.i.
     *
     * \pre
     * <em>Cert</em>
     * \post
     * Retorna XX si el sexe del individu en p.i. es femeni i XY si el sexe del individu en p.i. es masculi
     *
     */
    string retornSexe() const;

    /** @brief Operacio de consulta dels pares del individu en p.i.
     *
     * @return
     *
     * \pre
     * <em>Cert</em>
     * \post
     * Retorna si el individu en p.i. te pares o no.
     *
     */
    bool tePares() const;


    /**@brief Operacio de consulta de cromosomes sexuals del individu en p.i.
     *
     * @param primerOSegon
     * @return
     *
     * \pre
     * <strong>primerOSegon</strong> es 1 o 0
     * \post
     * Retorna el primer cromosoma sexual del individu en p.i. si <strong>primerOSegon</strong> es 0, el segon cromosoma sexual si <strong>primerOSegon</strong> es 1
     *
     */
    cromosoma retornCromosomaSexual(int primerOSegon) const;

    /** @brief Operacio de consulta de cromosomes normals del individu en p.i.
     *
     * @param numDePar
     * @param primerOSegon
     * @return
     *
     * \pre
     * <strong>numDePar</strong> es mes petit que la longitud del cromosoma i <strong>primerOSegon</strong> es 0 o 1
     * \post
     * Retorna el primer cromosoma normal del individu en p.i. si <strong>primerOSegon</strong> es 0, el segon cromosoma sexual si <strong>primerOSegon</strong> es 1 del par de cromosomes que has indicat.
     *
     */
    cromosoma retornCromosomaNormal(int numDePar, int primerOSegon) const;

    /**@brief Operacio de consulta de la mare del p.i.
     *
     * @return
     *
     * \pre
     * <em>Cert</em>
     * \post
     * Retorna el nom de la mare del individu en p.i.
     *
     */
    string retornNomMare() const;

    /**@brief Consultora del nom del pare del p.i.
     *
     * @return
     *
     * \pre
     * <em>Cert</em>
     * \post
     * Retorna el nom del pare del individu en p.i.
     *
     */
    string retornNomPare() const;

    //Escriure
    /**@brief Operacio de escritura del individu en p.i.
     *
     * \pre
     * <em>Cert</em>
     * \post
     * Escriu tot el genoma del individu en p.i.
     *
     */
    void escriure() const ;
};


#endif //PRO2_INDIVIDUO_H
