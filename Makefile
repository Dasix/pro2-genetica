OPCIONS = -D_JUDGE_ -D_GLIBCXX_DEBUG -O2 -Wall -Wextra -Wno-uninitialized -Wno-sign-compare -std=c++0x


program.exe: individuo.o especie.o cromosoma.o arbreGen.o program.o
	g++ -o program.exe program.o individuo.o especie.o cromosoma.o arbreGen.o

especie.o:  especie.cc especie.hh
	g++ -c especie.cc $(OPCIONS)

individuo.o: individuo.cc individuo.hh
	g++ -c individuo.cc $(OPCIONS)

cromosoma.o:  cromosoma.cc cromosoma.hh
	g++ -c cromosoma.cc $(OPCIONS)

arbreGen.o:  arbreGen.cc arbreGen.hh
	g++ -c arbreGen.cc Arbre.hh PRO2Excepcio.hh $(OPCIONS)

program.o: program.cc
	g++ -c program.cc $(OPCIONS)


clean:
	rm *.o
	rm *.exe
	rm *.hh.gch