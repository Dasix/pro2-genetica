//
// Created by david on 19/04/2017.
//

/** @file especie.hh
 * @brief Especificacio clase especie
 */

#ifndef PRO2_ESPECIE_H
#define PRO2_ESPECIE_H

#ifndef NO_DIAGRAM
#include <iostream>
#include <map>
#include <vector>
#include <list>
using namespace std;
#endif

#include "individuo.hh"

    /**@class especie
     *
     * @brief
     * Clase especie.
     * Representa una especie sencera amb tots els seus indivus
     *
     */

class especie {
private:
    map<string, individuo> persones;
    vector<int> longCromosomes;
    int numCromosomes;

    int lx;
    int ly;
    int l0;

    /**@brief Retorna si nom1 i nom2 són familia
     *
     * @param nom1
     * @param nom2
     * @return
     *
     * \pre
     * <strong>nom1</strong> i <strong>nom2</strong> existeixen
     * \post
     * Retorna: true si son familiars, false si no ho son
     *
     */
    bool esFamilia(string nom1, string nom2);

    /**@brief Retorna si nom1 i nom2 són reproduibles
     *
     * @param nom1
     * @param nom2
     * @return
     *
     * \pre
     * <strong>nom1</strong> i <strong>nom2</strong> existeixen
     * \post
     * Retorna: true si son reproduibles, false si no ho son
     *
     */
    bool esReproduible(string nom1, string nom2);

    /** @brief Consultora de si <strong> nom1 </strong> es antecessor de <strong>nom2</strong>
     *
     * @param nom1
     * @param nom2
     * @return
     *
     * \pre Existeixen <strong>nom1</strong> i <strong>nom2</strong>
     * \post Retorna: true si <strong>nom1</strong> es antecessor de <strong>nom2</strong>, false en altres casos
     *
     */
    bool esAntecessor(string nom1, string nom2);

public:

    /**@brief Retorna si nom ja existeix en la especie
     *
     * @param nom
     * @return
     *
     * \pre
     * <em>Cert</em>
     * \post
     * Retorna: true si <strong>nom</strong> ja existeix en la especie, false si no
     *
     */
    bool jaExisteix(string nom) const;

    /** @brief Constructora buida de especie
     *
     */
    especie();

    /**@brief Destructora
     *
     */
    ~especie();

    /** @brief Operacio de lectura de la especie
     *
     * \pre
     * <em>Cert</em>
     * \post
     * Les condicions de la especie en p.i. estan omplertes
     *
     */
    void llegirEspecie();

    /**@brief Operacio de creacio de individus
     *
     * \pre
     * <em>Cert</em>
     * \post
     * Afegeix a la especie en p.i. el numero de individus que vulguis
     *
     */

    void afegirIndividusPrincipals();

    /** @brief Operacio de creacio de individus
     *
     * \pre
     * <em>Cert</em>
     * \post
     * Afegeix a la especie en p.i. un nou individu sense pares si no existeix. Si existeix retorna "error".
     *
     */
    void afegirIndividu();

    /** @brief Operacio de reproduccio de individus
     *
     * \pre
     * <em>Cert</em>
     * \post
     * Afegeix a la especie en p.i. un nou individu creat a partir de dos altres individus de la especie en p.i si els
     * altres individus seleccionats no son familia i son de diferent sexe i existeixen. Si no existeixen o el individu nou ja existeix retorna "error",
     * si son familia o no son de diferent sexe retorna "no es posible reproduccion"
     *
     */
    void reproduirIndividu();


    /** @brief Operacio de escritura de tots els individus de la especie en p.i.
     *
     * \pre
     * <em>Cert</em>
     * \post
     * Escriu el nom de tots els individus de la especie en p.i. ordenats alfabeticament.
     *
     */
    void escriureEspecie();

    /** @brief Operacio de escritura de genotip
     *
     * \pre
     * <em>Cert</em>
     * \post
     * Escriu el genotip del individu que indiquis de la especie en p.i si existeix. Si no existeix retorna "error".
     *
     */
    void escriureGenotip();

    /** @brief Operacio de consulta de un individu de la especie en p.i.
     *
     * @param nom
     * @return
     *
     * \pre
     * L'individu nom existeix en la especie en p.i.
     * \post
     * Retorna l'individu <strong>nom</strong> de la especie en p.i.
     *
     */
    individuo retornIndividuo(string nom);
};


#endif //PRO2_ESPECIE_H
