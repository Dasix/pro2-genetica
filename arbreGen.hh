//
// Created by david on 25/04/2017.
//

/** @file arbreGen.hh
 * @brief Especificacio clase arbreGen
 */

#ifndef PRO2_ARBREGEN_H
#define PRO2_ARBREGEN_H

#ifndef NO_DIAGRAM
using namespace std;
#endif

#include "especie.hh"
#include "Arbre.hh"

/**@class arbreGen
 *
 * @brief
 * Clase Arbre Generador.
 * Clase per crear i completar els arbres Genealogics
 *
 */

class arbreGen {
private:

    /** @brief Operacio d'escriptura d'un arbre genealogic
     *
     * @param nom
     * @param esp
     *
     * \pre
     * L'individu <strong>nom</strong> existeix en la especie <strong>esp</strong>
     * \post
     * Escriu l'arbre genealogic complet de <strong>nom</strong> per nivells
     *
     */
    void escriureArbreGen(string nom, especie& esp);

    /** @brief Operacio de creacio d'un arbre genealogic en forma de Arbre
     *
     * @param arbre
     * @param nom
     * @param esp
     *
     * \pre
     * L'individu <strong>nom</strong> existeix en <strong>esp</strong> i l'Arbre <strong>arbre</strong> esta buit
     * \post
     * L'Arbre <strong>arbre</strong> queda omplert amb tot l'arbre genealogic de <strong>nom</strong>
     *
     */
    void crearArbre(Arbre<string>& arbre, string nom,especie& esp);

    /** @brief Operacio de lectura del arbre incomplet en forma de Arbre
     *
     * @param arbre
     *
     * \pre
     * <strong>arbre</strong> esta buit
     * \post
     * L'Arbre <strong>arbre</strong> queda omplert amb l'arbre que han escrit
     *
     */
    void llegirArbre(Arbre<string>& arbre);

    /** @brief Retorna si dos arbres son parcials
     *
     * @param arbreParcial
     * @param arbreTotal
     * @return
     *
     * \pre
     * <em>Cert</em>
     * \post
     * Retorna: true si <strong>arbreParcial</strong> es un arbre parcial de <strong>arbreTotal</strong>, false si no ho es
     */
    bool esParcial(Arbre<string>& arbreParcial, Arbre<string>& arbreTotal) const;

    /**@brief Operacio d'escriptura del complet del arbre parcial
     *
     * @param arbreIncomplet
     * @param arbreComplet
     *
     * \pre
     * <strong>arbreIncomplet</strong> i <strong>arbreComplet</strong> no son buits
     * \post
     * Escriu <strong>arbreComplet</strong> en preordre. Si l'element esta en <strong>arbreComplert</strong> i no en <strong>arbreIncomplet</strong> s'escriu marcat per * *, en altre cas s'escriu normal.
     *
     */
    void escrituraArbreParcial(Arbre<string>& arbreIncomplet, Arbre<string>& arbreComplet) const;

    /** @brief Operacio de escriptura d'arbreComplet
     *
     * @param arbreComplet
     *
     * \pre
     * <strong>arbreComplert</strong> no es buit
     * \post
     * Escriu <strong>arbreComplert</strong> marcant tots els elements amb * *.
     *
     */
    void escriureInexisten(Arbre<string>& arbreComplet) const;

public:

    /** @brief Constructora
     *
     */
    arbreGen();

    /** @brief Destructora
     *
     */
    ~arbreGen();

    /** @brief Operacio de escritura del arbre genealogic
     *
     * \pre
     * <em>Cert</em>
     * \post
     * Escriu l'arbre geneologic del individu de <strong>esp</strong> que indiquis
     *
     */
    void escriureArbreGen(especie& esp);


    /** @brief Operacio de completar un arbre geneologic incomplet.
     *
     * @param esp
     *
     * \pre
     * <em>Cert</em>
     * \post
     * Escriu l'arbre geneologic complet marcant quins elements li faltan al arbre incomplet que hagis introduit.
     * Si l'arbre incomplet introduit no existeix en <strong>esp</strong> retorna "no es arbol parcial"
     *
     */
    void completarArbreGen(especie& esp);
};


#endif //PRO2_ARBREGEN_H
