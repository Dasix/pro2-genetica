//
// Created by david on 09/04/2017.
//

/** @file cromosoma.hh
 * @brief Especificacio clase cromosoma
 */

#ifndef PRO2_CROMOSOMA_H
#define PRO2_CROMOSOMA_H

#ifndef NO_DIAGRAM
#include <iostream>
#include <vector>
using namespace std;
#endif

/**@class cromosoma
   *
   * @brief
   * Clase cromosoma.
   * Representa un cromosoma del genotip
   *
   */

class cromosoma {
private:
    vector<int> cromo;
    bool  cromosomaSexual= false;
    char tipoDeSexual;

public:
    /** @brief Constructora
     *
     */
    cromosoma();

    /** @brief Constructora amb longitud del cromosoma
     *
     * @param longitud
     *
     */
    cromosoma(int longitud);

    /**@brief Destructora
     *
     */
    ~cromosoma();

    /** @brief Operacio de lectura de un cromosoma
     *
     * @param longitud
     *
     * \pre
     * <em>Cert</em>
     * \post
     * El cromosoma p.i. de longitud <strong>longitud</strong> queda omplert amb els seus gens.
     *
     */
    void llegirCromosoma(int longitud);

    /** @brief Operacio de lectura de un cromosoma sexual
     *
     * @param longitud
     * @param tipo
     *
     * \pre
     * <em>Cert</em>
     * \post
     * El cromosoma p.i. de longitud <strong>longitud</strong> queda omplert amb els seus gens i tipo de cromosoma ( X o Y ).
     *
     */
    void llegirCromosomaSexual(int longitud, char tipo);

    //Returns
    /** @brief Operacio de consulta
     *
     * @return
     *
     * \pre
     * <em>Cert</em>
     * \post
     * Retorna la mida del Cromosoma en p.i.
     *
     */
    int retornSize() const ;

    /** @brief Operacio de consulta del valor dels gens
     *
     * @param x
     * @return
     *
     * \pre
     * X es mes petit que la longitud del cromosoma en p.i.
     * \post
     * Retorna el valor del gen en la posicio <strong>x</strong> del cromosoma en p.i.
     *
     */
    int retornValor(int x) const ;

    /**@brief Operacio de modificacio del Cromosoma
     *
     * @param x
     * @param v
     *
     * \pre
     * x es mes petit que la longitud del cromosoma en p.i.
     * \post
     * la posicio <strong>v</strong> del cromosoma en p.i. te valor <strong>v</strong>
     *
     */
    void cambiarValor(int x, int v);

    /**@brief Operacio de consulta del tipus de cromosoma Sexual
     *
     * @return
     * \pre
     * El cromosoma en p.i. es sexual
     * \post
     * Retorna el tipus de cromosoma Sexual que es el cromosoma en p.i.
     *
     */
    char retornTipoDeSexual() const;

    //Escritors
    /**@brief Operacio de escritura del cromosoma en p.i.
     *
     * \pre
     * <em>Cert</em>
     * \post
     * Escriu els gens del cromosoma en p.i.
     *
     */
    void escriure() const;
};


#endif //PRO2_CROMOSOMA_H
