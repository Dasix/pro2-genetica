//
// Created by david on 09/04/2017.
//

/**@file cromosoma.cc
 *
 * @cromosoma
 */


#include "cromosoma.hh"

cromosoma::cromosoma() {}

cromosoma::cromosoma(int longitud){
    cromo = vector<int>(longitud);
}

cromosoma::~cromosoma(){}

void cromosoma::llegirCromosoma(int longitud) {
    cromo = vector<int>(longitud);
    cromosomaSexual= false;

    for(int i=0; i < longitud; ++i){
        int n;
        cin >> n;//1 o 0
        cromo[i]=n;
    }
}

void cromosoma::llegirCromosomaSexual(int longitud, char tipo) {
    cromo = vector<int>(longitud);
    cromosomaSexual= true;
    tipoDeSexual=tipo;

    for(int i=0; i < longitud; ++i){
        int n;
        cin >> n;//1 o 0
        cromo[i]=n;
    }
}


//Returns
int cromosoma::retornSize() const {
    return cromo.size();
}

int cromosoma::retornValor(int x) const {
    return cromo[x];
}

void cromosoma::cambiarValor(int x, int v) {
    cromo[x]=v;
}

char cromosoma::retornTipoDeSexual() const {
    return tipoDeSexual;
}

//Escritors
void cromosoma::escriure() const {
    if(cromosomaSexual){
        cout << "  " << tipoDeSexual << ":";
    }
    for(int i=0; i < cromo.size(); ++i){
        cout << " " <<cromo[i];
    }
    cout << endl;
}