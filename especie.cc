//
// Created by david on 19/04/2017.
//

/**@file especie.cc
 *
 *
 * @especie
 */

#include "especie.hh"
#include "PRO2Excepcio.hh"

//Private Returns

bool especie::esFamilia(string nom1, string nom2){

    if(!persones[nom1].tePares() && !persones[nom2].tePares()){
        return false;
    }

    string nomPare1=persones[nom1].retornNomPare();
    string nomMare1=persones[nom1].retornNomMare();
    string nomPare2=persones[nom2].retornNomPare();
    string nomMare2=persones[nom2].retornNomMare();

    if(nomMare1==nomMare2 || nomPare1==nomPare2 ){
        return true;
    }

    return (esAntecessor(nom1,nom2) || esAntecessor(nom2,nom1));
}

bool especie::esReproduible(string nom1, string nom2){
    return ((persones[nom1].retornSexe()=="XX"  && persones[nom2].retornSexe()== "XY") && !esFamilia(nom1,nom2));
}

bool especie::jaExisteix(string nom) const {
    return !(persones.find(nom) == persones.end());
}

bool especie::esAntecessor(string nom1, string nom2){
    individuo persona = persones[nom2];
    if(persona.tePares()){
        string pare = persona.retornNomPare();
        string mare = persona.retornNomMare();
        if(nom1==pare) return true;
        if(nom1==mare) return true;
        return (esAntecessor(nom1,pare) || esAntecessor(nom1,mare));
    }else{
        return false;
    }
}

especie::especie() {}

especie::~especie(){}

void especie::llegirEspecie() {
    cin >> numCromosomes;
    cin >> l0;
    longCromosomes = vector<int>(numCromosomes);
    for(int i = 0; i < numCromosomes; ++i){
        int n;
        cin >> n;
        longCromosomes[i] = n;
    }
    cin >> lx;
    cin >> ly;
}

void especie::afegirIndividusPrincipals() {
    int n;
    cin >> n;
    for(int i=0; i < n; ++i){
        string nom;
        cin >> nom;
        if(!jaExisteix(nom)) {
            individuo novaPersona(numCromosomes, longCromosomes, ly, lx, nom);
            persones[nom] = novaPersona;
        }else{
            //throw PRO2Excepcio ("error");
            cout << "  error" << endl;
        }
    }
}

void especie::afegirIndividu() {
    string nom;
    cin >> nom;
    cout << "anadir_individuo " << nom << endl;
    if(!jaExisteix(nom)) {
        individuo novaPersona(numCromosomes, longCromosomes, ly, lx, nom);
        persones[nom] = novaPersona;
    }else{
        //throw PRO2Excepcio ("error");
        cout << "  error" << endl;
    }
}

void especie::reproduirIndividu() {
    string nomMare,nomPare,nom;
    cin >> nomMare >>  nomPare >> nom;
    cout << "reproduccion_sexual " << nomMare << " " <<  nomPare << " " <<nom << endl;
    if(jaExisteix(nomMare) && jaExisteix(nomPare) && !jaExisteix(nom)){
        if(esReproduible(nomMare,nomPare)){
            individuo novaPersona(persones[nomMare],persones[nomPare],nom,longCromosomes,numCromosomes,l0);
            persones[nom]=novaPersona;
        }else{
            //throw PRO2Excepcio ("no es posible reproduccion");
            cout << "  no es posible reproduccion" << endl;
        }
    }else{
        //throw PRO2Excepcio ("error");
        cout << "  error" << endl;
    }
}

void especie::escriureEspecie() {
    cout << "escribir_poblacion" << endl;
    int i=0;
    for(map<string, individuo> :: const_iterator iter = persones.begin(); iter != persones.end(); ++iter){
        individuo tmp = (*iter).second;
        string nom = tmp.retornNom();
        if(jaExisteix(nom)) {
            cout << "  " << nom << " " << tmp.retornSexe() << " " << "(" << tmp.retornNomPare() << ","
                 << tmp.retornNomMare() << ")" << endl;
        }
        ++i;
    }
}

void especie::escriureGenotip() {
    string nom;
    cin >> nom;
    cout << "escribir_genotipo " << nom << endl;
    if(jaExisteix(nom)){
        persones[nom].escriure();
    }else{
        //throw PRO2Excepcio ("error");
        cout << "  error" << endl;
    }
}

individuo especie::retornIndividuo(string nom)  {
    return persones[nom];
}