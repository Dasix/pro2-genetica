// Per poder fer git
// Created by david on 09/04/2017.
//

/**@file individuo.cc
 *
 * @individuo
 */

#include "individuo.hh"

void individuo::creuarCromosomes(cromosoma &primer, cromosoma &segon, int puntDeTall, int midaMax) {
    cromosoma tmp = primer;
    for (int i = puntDeTall; i < midaMax; ++i) {
        primer.cambiarValor(i,segon.retornValor(i));
        segon.cambiarValor(i,tmp.retornValor(i));
    }
}

individuo::individuo() {}

individuo::individuo(const int NumCromosomas,const vector<int>& longCromosomas,const int ly,const int lx,const string nom){

    nomMare = "$";
    nomPare = "$";
    pares = false;
    this->nom=nom;

    char n;
    cin >> n;

    if(n=='X'){
        cromosomasSexuales.first.llegirCromosomaSexual(lx,'X');
        cromosomasSexuales.second.llegirCromosomaSexual(lx,'X');
        sexe="XX";
    }else if(n == 'Y'){
        cromosomasSexuales.first.llegirCromosomaSexual(lx,'X');
        cromosomasSexuales.second.llegirCromosomaSexual(ly,'Y');
        sexe="XY";
    }else{
        cout << "Aixo no es un tipus de cromosoma" << endl;
    }

    cromosomasNormales = vector<pair<cromosoma,cromosoma>>(NumCromosomas);
    for(int i=0; i < NumCromosomas; ++i){
        cromosomasNormales[i].first.llegirCromosoma(longCromosomas[i]);
        cromosomasNormales[i].second.llegirCromosoma(longCromosomas[i]);
    }
}

individuo::individuo(const individuo madre, const individuo padre, const string nom,const vector<int>& longCromosoma, const int numCromosomes,const int l0) {

    nomMare=madre.retornNom();
    nomPare=padre.retornNom();
    this->nom=nom;
    pares=true;

    int puntDeTall;
    int cromosomaAAgafarDelPare;
    int cromosomaAAgafarDeLaMare;

    cin >> cromosomaAAgafarDeLaMare;
    cin >> cromosomaAAgafarDelPare;
    cin >> puntDeTall;

    cromosomasSexuales.first=madre.retornCromosomaSexual(cromosomaAAgafarDeLaMare);
    cromosomasSexuales.second=padre.retornCromosomaSexual(cromosomaAAgafarDelPare);
    creuarCromosomes(cromosomasSexuales.first, cromosomasSexuales.second, puntDeTall,
                     l0);

    if(cromosomaAAgafarDelPare==1){
        sexe="XY";
    }else{
        sexe="XX";
    }

    cromosomasNormales = vector<pair<cromosoma,cromosoma>>(numCromosomes);
    for(int i=0; i < numCromosomes; ++i){

        cin >> cromosomaAAgafarDeLaMare;
        cin >> cromosomaAAgafarDelPare;
        cin >> puntDeTall;

        //Creació del creuament de cromosomes
        cromosomasNormales[i].first=madre.retornCromosomaNormal(i,cromosomaAAgafarDeLaMare);
        cromosomasNormales[i].second=padre.retornCromosomaNormal(i,cromosomaAAgafarDelPare);

        creuarCromosomes(cromosomasNormales[i].first,cromosomasNormales[i].second,puntDeTall,longCromosoma[i]);
    }

}

individuo::~individuo() {}

//Return
string individuo::retornSexe() const {
    return sexe;
}

bool individuo::tePares() const {
    return pares;
}

string individuo::retornNom() const{
    return nom;
}

cromosoma individuo::retornCromosomaSexual(int primerOSegon) const{
    if(primerOSegon==0){
        return cromosomasSexuales.first;
    }else{
        return cromosomasSexuales.second;
    }
}

cromosoma individuo::retornCromosomaNormal( int numDePar, int primerOSegon) const{
    if(primerOSegon==0){
        return cromosomasNormales[numDePar].first;
    }else{
        return cromosomasNormales[numDePar].second;
    }
}

string individuo::retornNomMare() const {
    return nomMare;
}

string individuo::retornNomPare() const {
    return nomPare;
}

//Escriure
void individuo::escriure() const {
    cromosomasSexuales.first.escriure();
    cromosomasSexuales.second.escriure();

    for(int i=0; i < cromosomasNormales.size(); ++i){
        cout << "  " << i+1 << ".1:";
        cromosomasNormales[i].first.escriure();
        cout << "  " << i+1 << ".2:";
        cromosomasNormales[i].second.escriure();
    }

}
