
/**@file program.cc
 *
 */

#ifndef NO_DIAGRAM
#include <iostream>
using namespace std;
#endif

#include "especie.hh"
#include "arbreGen.hh"

/** @mainpage
 *
 *
 * Exemple de diseny moduilar: Experiments al laboratori <br>
 * En aquest exemple es crea un programa modular que ofereix un menu d'opcions per fer experiments genetics. <br>
 *
 * S'han creat clases noves amb el nom de: <b><em>especie, arbreGen, individuo, cromosoma</em></b>.
 *
 *
 *
 * Fet per David Sirera Pulido
 */

int main() {

    especie novaEspecie;
    novaEspecie.llegirEspecie();
    novaEspecie.afegirIndividusPrincipals();

    string bucle;
    cin >> bucle;

    while(bucle != "acabar"){

        if(bucle == "anadir_individuo" ){
            novaEspecie.afegirIndividu();
        }else if(bucle == "reproduccion_sexual" ){
            novaEspecie.reproduirIndividu();
        }else if(bucle == "escribir_arbol_genealogico" ){
            arbreGen nouArbre;
            nouArbre.escriureArbreGen(novaEspecie);
        }else if(bucle == "completar_arbol_genealogico" ){
            arbreGen nouArbre;
            nouArbre.completarArbreGen(novaEspecie);
        }else if(bucle == "escribir_genotipo" ){
            novaEspecie.escriureGenotip();
        }else if(bucle == "escribir_poblacion" ){
            novaEspecie.escriureEspecie();
        }
        cin >> bucle;
    }
    cout << "acabar" << endl;
    return 0;
}