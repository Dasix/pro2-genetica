//
// Created by david on 25/04/2017.
//

/** @file arbreGen.cc
 *
 * @arbreGen
 */

#include "arbreGen.hh"


void arbreGen::escriureArbreGen(string nom, especie &esp) {
    int nivell=0;

    list<string> nivellActual = {nom};
    list<string> nivellSeguent;

    while(!nivellActual.empty()){
        cout << "  Nivel " << nivell << ":";
        for (list<string>::const_iterator it = nivellActual.begin(); it != nivellActual.end(); ++it) {

            cout << " ";
            cout << (*it);

            individuo madre, padre, jo;
            jo = esp.retornIndividuo(*it);
            padre = esp.retornIndividuo(jo.retornNomPare());
            madre = esp.retornIndividuo(jo.retornNomMare());

            if (jo.tePares()) {
                nivellSeguent.insert(nivellSeguent.end(), padre.retornNom());
                nivellSeguent.insert(nivellSeguent.end(), madre.retornNom());
            }
        }
        cout << endl;

        nivellActual=nivellSeguent;
        nivellSeguent={};
        nivell++;
    }
}

void arbreGen::crearArbre(Arbre<string>& arbre, string nom, especie& esp) {
    Arbre<string> nou1, nou2;
    individuo personaActual = esp.retornIndividuo(nom);
    if(nom != "$"){
        crearArbre(nou1,personaActual.retornNomPare(),esp);
        crearArbre(nou2,personaActual.retornNomMare(),esp);
    }
    arbre.plantar(nom,nou1,nou2);
}

void arbreGen::llegirArbre(Arbre<string>& arbre) {
    Arbre<string> nou1, nou2;
    string nom;
    cin >> nom;

    if(nom != "$"){
        llegirArbre(nou1);
        llegirArbre(nou2);
    }
    arbre.plantar(nom,nou1,nou2);
}

bool arbreGen::esParcial(Arbre<string>& arbreParcial, Arbre<string>& arbreTotal) const {
    bool parcial = true;
    if (!arbreParcial.es_buit() && !arbreTotal.es_buit()) {
        Arbre<string> aa1, aa2, bb1, bb2;
        Arbre<string> arbreParcial2 = arbreParcial;
        Arbre<string> arbreTotal2 = arbreTotal;
        bool comprobacioNivell = (arbreParcial.arrel() == "$"  || (arbreParcial.arrel() == arbreTotal.arrel()));
        arbreParcial2.fills(aa1, aa2);
        arbreTotal2.fills(bb1, bb2);
        parcial = (esParcial(aa1, bb1) && esParcial(aa2, bb2)) && comprobacioNivell;
    }
    return parcial;
}

void arbreGen::escrituraArbreParcial(Arbre<string>& arbreIncomplet, Arbre<string>& arbreComplet) const {
    if(arbreIncomplet.arrel() == arbreComplet.arrel()){
        cout << " " << arbreComplet.arrel();
        if(arbreComplet.arrel() != "$") {
            Arbre<string> aa1, aa2, bb1, bb2;
            arbreIncomplet.fills(aa1, aa2);
            arbreComplet.fills(bb1, bb2);
            escrituraArbreParcial(aa1, bb1);
            escrituraArbreParcial(aa2, bb2);
        }
    }else{
        escriureInexisten(arbreComplet);
    }
}

void arbreGen::escriureInexisten(Arbre<string>& arbreComplet) const {
    if(arbreComplet.arrel() == "$"){
        cout << " $";
    }else{
        cout << " *" << arbreComplet.arrel() << "*";
        Arbre<string> aa1,aa2;
        arbreComplet.fills(aa1,aa2);
        escriureInexisten(aa1);
        escriureInexisten(aa2);
    }
}

arbreGen::arbreGen() {}

arbreGen::~arbreGen() {}

void arbreGen::escriureArbreGen(especie& esp) {
    string nom;
    cin >> nom;
    cout << "escribir_arbol_genealogico " << nom << endl;
    if(esp.jaExisteix(nom)){
        escriureArbreGen(nom, esp);
    }else{
        //throw PRO2Excepcio ("error");
        cout << "  error" << endl;
    }
}

void arbreGen::completarArbreGen(especie& esp) {
    Arbre<string> arbreEscrit, arbreTotal;
    llegirArbre(arbreEscrit);

    string nom = arbreEscrit.arrel();
    cout << "completar_arbol_genealogico "<< nom << endl;

    if(esp.jaExisteix(nom)){
        crearArbre(arbreTotal,nom,esp);
        if(esParcial(arbreEscrit, arbreTotal)){
            cout << " "; //Espai per output
            escrituraArbreParcial(arbreEscrit,arbreTotal);
            cout << endl;
        }else{
            cout << "  no es arbol parcial" << endl;
        }
    }else{
        cout << "  no es arbol parcial" << endl;
    }

}